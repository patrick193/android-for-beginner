package hw1;

import java.util.Scanner;

public class Even {

    /**
     *
     * @param scanner getting scanner instance from main program;
     */
    public static void main(Scanner scanner) {
        while (true) {
            try {
                String result = "Result is ";
                System.out.println("Enter the number: ");
                if (!scanner.hasNextInt()) {
                    Even.log("Input should be a number and  between " + Integer.MAX_VALUE + " and " + Integer.MIN_VALUE);
                    break;
                }
                int input = Math.abs(scanner.nextInt());
                String out = result + (isEven(input) ? "even" : "not even");
                Even.log(out);
            } catch (Exception ex) {
                Even.log(ex.getMessage());
            }
        }
    }

    private static void log(String out) {
        System.out.println(out);
    }

    private static boolean isEven(int number) {
        return (number % 2) == 0;
    }
}
