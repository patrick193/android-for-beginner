import  hw1.Even;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            System.out.println("Choose a homework number: ");
            int hwNumber = scanner.nextInt();
            Main.start(hwNumber);
        } catch (InputMismatchException ex) {
            System.out.println("The answer can be only a number;");
            System.out.println(ex.getMessage());
        }

    }

    /**
     * hahha just brute force :D
     * @param hwNumber number of HW
     */
    private static void start(int hwNumber) {
        switch (hwNumber) {
            case 1: {
                System.out.println("Even hw start!");
                Even.main(scanner);
                break;
            }
            default: {
                System.out.println("no hw yet");
                break;
            }
        }
    }
}
